// Generated code from Butter Knife. Do not modify!
package com.rti.pasti.menu.akun.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.rti.pasti.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AkunActivity_ViewBinding implements Unbinder {
  private AkunActivity target;

  private View view2131230760;

  private View view2131230801;

  @UiThread
  public AkunActivity_ViewBinding(AkunActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AkunActivity_ViewBinding(final AkunActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnTes, "field 'btnTes' and method 'setOnClick'");
    target.btnTes = Utils.castView(view, R.id.btnTes, "field 'btnTes'", Button.class);
    view2131230760 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.setOnClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.edName, "field 'edName' and method 'setOnClick'");
    target.edName = Utils.castView(view, R.id.edName, "field 'edName'", EditText.class);
    view2131230801 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.setOnClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AkunActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnTes = null;
    target.edName = null;

    view2131230760.setOnClickListener(null);
    view2131230760 = null;
    view2131230801.setOnClickListener(null);
    view2131230801 = null;
  }
}
