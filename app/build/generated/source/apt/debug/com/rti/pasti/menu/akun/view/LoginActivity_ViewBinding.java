// Generated code from Butter Knife. Do not modify!
package com.rti.pasti.menu.akun.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.rti.pasti.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view2131230759;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btnLogin, "field 'btnLogin' and method 'btnLoginOnClick'");
    target.btnLogin = Utils.castView(view, R.id.btnLogin, "field 'btnLogin'", Button.class);
    view2131230759 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.btnLoginOnClick();
      }
    });
    target.edUsername = Utils.findRequiredViewAsType(source, R.id.edUsername, "field 'edUsername'", EditText.class);
    target.edPassword = Utils.findRequiredViewAsType(source, R.id.edPassword, "field 'edPassword'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnLogin = null;
    target.edUsername = null;
    target.edPassword = null;

    view2131230759.setOnClickListener(null);
    view2131230759 = null;
  }
}
