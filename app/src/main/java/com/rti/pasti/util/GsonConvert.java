package com.rti.pasti.util;

import com.google.gson.Gson;

/**
 * Created by firmanmac on 4/24/18.
 */

public class GsonConvert {

    static Gson gson = new Gson();

    public static String toJson(Object object){
        return gson.toJson(object);
    }

    public static Object toObject(String json, Class<Object> object){
        return gson.fromJson(json, object);
    }

}
