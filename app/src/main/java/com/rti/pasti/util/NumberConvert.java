package com.rti.pasti.util;

import android.util.Log;

import java.text.NumberFormat;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by firmanmac on 4/24/18.
 */

public class NumberConvert {

    public int persentaseRating(Double rating){
        Log.d("tesGo", String.valueOf(rating));
        Log.d("tesGo", String.valueOf((rating * 10)));
        return (int) (rating * 10);
    }

    public String format_angka(String angka){
        return NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(angka)));
    }

    public String format_angka(int angka){
        return NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(angka)));
    }

}
