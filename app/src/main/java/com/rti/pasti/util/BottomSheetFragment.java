package com.rti.pasti.util;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class BottomSheetFragment extends BottomSheetDialogFragment {

    //Test bottom

    private int dialog_top_sheet_menu;

    public BottomSheetFragment() {}

    @SuppressLint("ValidFragment")
    public BottomSheetFragment(int dialog_top_sheet_menu) {
        this.dialog_top_sheet_menu = dialog_top_sheet_menu;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(dialog_top_sheet_menu, container, false);
    }
}