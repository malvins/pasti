package com.rti.pasti.util;

import com.rti.pasti.conf.MyConf;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public class RetrofitUtil {
    private static Retrofit outInstance;
    private static String baseUrl = MyConf.BASE_URL;

    public static Retrofit getInstance(){
        if (outInstance == null)
            outInstance = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        return outInstance;
    }

}
