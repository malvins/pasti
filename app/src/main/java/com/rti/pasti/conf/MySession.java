package com.rti.pasti.conf;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public class MySession {
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;

    public static final String session_mode_connection = "session_mode_connection";
    public static final String session_ip_address = "session_ip_address";
    public static final String session_branchid = "session_branchid";
    public static final String session_branchname = "session_branchname";
    public static final String session_branchaddress = "session_branchaddress";
    public static final String session_branchphone = "session_branchphone";
    public static final String session_comid = "session_comid";
    public static final String session_comname = "session_comname";
    public static final String session_userid = "session_userid";
    public static final String session_fullname = "session_fullname";
    public static final String session_rolename = "session_rolename";
    public static final String session_warehouseid = "session_warehouseid";
    public static final String session_divisionid = "session_divisionid";

    public static void beginInitialization(Context context){
        pref = context.getSharedPreferences("MySession", 0); // 0 - for private mode
        editor = pref.edit();
    }

    public static void setSessionGlobal(String key,String val){
        editor.putString(key, val);
        editor.commit();
    }

    public static String getSessionGlobal(String key){
        return pref.getString(key,null);
    }

    public static void clearSessionGlobal(String key){
        editor.remove(key);
        editor.commit();
    }

    public static void sessionClearEverything(){
        editor.clear();
        editor.commit();
    }
}
