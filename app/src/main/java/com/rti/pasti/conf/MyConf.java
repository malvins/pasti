package com.rti.pasti.conf;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public class MyConf {

    //public static final String BASE_URL = "http://192.168.1.107:8787";
    //public static final String USER_API = BASE_URL + "/user";

    // dummy
    public static final String BASE_URL = "http://toriatec.com";
    public static final String USER_API = BASE_URL + "/api-app";
    public static final String UMROH_API = BASE_URL + "/api-app/umroh.json";
}
