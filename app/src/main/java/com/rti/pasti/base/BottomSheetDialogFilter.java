package com.rti.pasti.base;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rti.pasti.R;
import com.rti.pasti.menu.home.view.activity.UmrohActivity;
import com.rti.pasti.util.NumberConvert;

/**
 * Created by firmanmac on 4/26/18.
 */

public class BottomSheetDialogFilter extends BottomSheetDialogFragment {

    SeekBar seekBarHargaPerOrang;
    TextView txtHargaPerOrang;
    NumberConvert numberConvert = new NumberConvert();
    RatingBar ratingFilter;
    Button btnFilterSelesai;
    MaterialSpinner spinnerJumlahOrang;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_bottom_sheet_filter, container, false);

        seekBarHargaPerOrang = (SeekBar)v.findViewById(R.id.seekBarHargaPerOrang);
        txtHargaPerOrang = (TextView) v.findViewById(R.id.txtHargaPerOrang);
        ratingFilter = (RatingBar)v.findViewById(R.id.ratingFilter);
        btnFilterSelesai = (Button)v.findViewById(R.id.btnFilterSelesai);
        spinnerJumlahOrang = (MaterialSpinner)v.findViewById(R.id.spinnerJumlahOrang);

        seekBarHargaPerOrang.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtHargaPerOrang.setText(String.valueOf(numberConvert.format_angka(i)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnFilterSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"rating terpilih = " + String.valueOf(ratingFilter.getRating()) , Toast.LENGTH_SHORT).show();
            }
        });

        spinnerJumlahOrang.setItems(UmrohActivity.getJumlahOrang());

        spinnerJumlahOrang.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                //country = ar_country_Id.get(position);
            }
        });

        return v;
    }

}
