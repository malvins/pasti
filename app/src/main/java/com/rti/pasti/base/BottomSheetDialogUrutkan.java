package com.rti.pasti.base;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.view.activity.UmrohActivity;

/**
 * Created by firmanmac on 4/26/18.
 */

public class BottomSheetDialogUrutkan extends BottomSheetDialogFragment {

    Button btnUrutanSelesai;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_bottom_sheet_urutkan, container, false);
        btnUrutanSelesai = (Button) v.findViewById(R.id.btnUrutanSelesai);
        btnUrutanSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UmrohActivity.closeBottomSheet();
            }
        });
        return v;
    }
}
