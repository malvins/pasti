package com.rti.pasti.menu.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.view.activity.UmrohActivity;
import com.rti.pasti.menu.main.model.HomeMenu;

import java.util.List;

/**
 * Created by firmanmac on 4/22/18.
 */

public class HomeMenuAdapter extends RecyclerView.Adapter<HomeMenuAdapter.MyViewHolder> {

    private Context context;
    private List<HomeMenu> homeMenu;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgHomeImg;
        TextView txtHomeNamaMenu;
        CardView cv_menu_home;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgHomeImg = (ImageView)itemView.findViewById(R.id.imgHomeImg);
            txtHomeNamaMenu = (TextView) itemView.findViewById(R.id.txtHomeNamaMenu);
            cv_menu_home = (CardView)itemView.findViewById(R.id.cv_menu_home);
        }
    }

    public HomeMenuAdapter(Context context, List<HomeMenu> homeMenu) {
        this.context = context;
        this.homeMenu = homeMenu;
    }

    @Override
    public HomeMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeMenuAdapter.MyViewHolder holder, int position) {

        HomeMenu homeIc = homeMenu.get(position);
        //holder.imgHomeImg.setImageDrawable(R.drawable.ic_brightness_1_black_24dp);
        holder.txtHomeNamaMenu.setText(homeIc.getNamaMenu());
        holder.cv_menu_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UmrohActivity.class);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeMenu.size();
    }

}
