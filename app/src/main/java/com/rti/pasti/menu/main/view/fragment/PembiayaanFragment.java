package com.rti.pasti.menu.main.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rti.pasti.R;


/**
 * Created by firmanmac on 4/22/18.
 */

public class PembiayaanFragment extends Fragment {

    private Context context = null;
    private String TAG = "ser_home";
    //private HomeService homeService;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_pembiayaan, container, false);
        getActivity().setTitle("Home");

        context = getActivity();

        //homeService = new HomeService();
        //homeService.getSessionTest(context);

        return rootView;
    }
}
