package com.rti.pasti.menu.main.model;

/**
 * Created by firmanmac on 4/22/18.
 */

public class HomeMenu {

    private int idMenu;
    private String namaMenu;
    private String gambarMenu;

    public HomeMenu() {
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getGambarMenu() {
        return gambarMenu;
    }

    public void setGambarMenu(String gambarMenu) {
        this.gambarMenu = gambarMenu;
    }
}
