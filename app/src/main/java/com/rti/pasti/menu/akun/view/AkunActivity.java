package com.rti.pasti.menu.akun.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rti.pasti.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunActivity extends AppCompatActivity {

    @BindView(R.id.btnTes)
    Button btnTes;

    @BindView(R.id.edName)
    EditText edName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.akun_activity);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnTes, R.id.edName})
    void setOnClick (View v){
        switch (v.getId()){
            case R.id.btnTes:
                btnTesOnClick();
                break;
            case R.id.edName:
                edNameOnClick();
                break;
            default:
                break;
        }

    }

    private void btnTesOnClick(){
        String nama = edName.getText().toString();
        Toast.makeText(this, nama, Toast.LENGTH_SHORT).show();
    }

    private void edNameOnClick(){
        Toast.makeText(this, "jangan dipencet ah", Toast.LENGTH_SHORT).show();
    }
}
