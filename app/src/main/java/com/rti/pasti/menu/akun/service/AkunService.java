package com.rti.pasti.menu.akun.service;

import com.rti.pasti.conf.MyConf;
import com.rti.pasti.menu.akun.model.ReqLogin;
import com.rti.pasti.menu.akun.model.ResLogin;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public interface AkunService {
    @POST(MyConf.UMROH_API+"/login.json")
    Observable<ResLogin> login(@Body ReqLogin reqLogin);
}
