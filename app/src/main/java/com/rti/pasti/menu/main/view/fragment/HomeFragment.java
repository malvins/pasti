package com.rti.pasti.menu.main.view.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.adapter.HomeBannerVpAdapter;
import com.rti.pasti.menu.main.adapter.HomeMenuAdapter;
import com.rti.pasti.menu.main.model.HomeMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by firmanmac on 4/22/18.
 */

public class HomeFragment extends Fragment {

    private Context context = null;
    private String TAG = "ser_home";
    //private HomeService homeService;

    private RecyclerView rvHomeMenu;
    private HomeMenuAdapter homeMenuAdapter;
    private List<HomeMenu> homeMenuList = new ArrayList<>();
    private ArrayList<String> ar_menu_img = new ArrayList<String>();
    private ArrayList<String> ar_menu_txt = new ArrayList<String>();
    Activity activity;
    RecyclerView.LayoutManager mLayoutManager;


    //@BindView(R.id.vp_home_banner)
    ViewPager vpHomeBanner;

    HomeBannerVpAdapter vpAdapter;
    String images[] = {
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtYtTw-5D8upMVI-Eexn7Azlt0m0ldtDW-eNUzeatOTIjEn3r4",
            "http://www.calderglen.s-lanark.sch.uk/wpMaths/wp-content/uploads/2017/03/Banner-5-e1488903930167.jpg",
            "https://evanslaboratory.files.wordpress.com/2016/10/home-banner-3.jpg?w=1200"
    };
    Timer timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        getActivity().setTitle("Home");

        context = getActivity();
        activity = getActivity();

        //ButterKnife.bind(activity);
        vpHomeBanner = (ViewPager)rootView.findViewById(R.id.vp_home_banner);
        vpAdapter = new HomeBannerVpAdapter(activity, images);
        vpHomeBanner.setAdapter(vpAdapter);

        timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 10000, 10000);

        rvHomeMenu = (RecyclerView) rootView.findViewById(R.id.rv_home_menu);
        homeMenuAdapter = new HomeMenuAdapter(context, homeMenuList);
        mLayoutManager = new GridLayoutManager(context, 3);
        rvHomeMenu.setLayoutManager(mLayoutManager);
        rvHomeMenu.setItemAnimator(new DefaultItemAnimator());
        rvHomeMenu.setAdapter(homeMenuAdapter);


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        fecthAllData();
    }

    public class MyTimerTask extends TimerTask {
        protected MyTimerTask() {
            super();
        }

        @Override
        public boolean cancel() {
            return super.cancel();
        }

        @Override
        public long scheduledExecutionTime() {
            return super.scheduledExecutionTime();
        }

        @Override
        public void run() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(vpHomeBanner.getCurrentItem()==0){
                        vpHomeBanner.setCurrentItem(1);
                    }else if(vpHomeBanner.getCurrentItem()==1){
                        vpHomeBanner.setCurrentItem(2);
                    }else{
                        vpHomeBanner.setCurrentItem(0);
                    }


                }
            });

        }
    }

    private void fecthAllData() {

        ar_menu_txt.clear();
        ar_menu_img.clear();
        ar_menu_txt.add("Umroh");
        ar_menu_txt.add("Asuransi");
        ar_menu_txt.add("Tabungan");
        ar_menu_txt.add("Investasi");
        ar_menu_txt.add("Manasik");
        ar_menu_img.add("-");
        ar_menu_img.add("-");
        ar_menu_img.add("-");
        ar_menu_img.add("-");
        ar_menu_img.add("-");

        for (int a = 0; a < ar_menu_txt.size(); a++) {
            HomeMenu homeMenu = new HomeMenu();
            homeMenu.setIdMenu(a);
            homeMenu.setNamaMenu(ar_menu_txt.get(a));
            homeMenu.setGambarMenu(ar_menu_img.get(a));
            homeMenuList.add(homeMenu);
        }
        homeMenuAdapter.notifyDataSetChanged();

        loadDataSlider();

    }

    private void loadDataSlider() {

    }
}
