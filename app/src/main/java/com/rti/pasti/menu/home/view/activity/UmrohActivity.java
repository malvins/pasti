package com.rti.pasti.menu.home.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rti.pasti.R;
import com.rti.pasti.base.BottomSheetDialogFilter;
import com.rti.pasti.base.BottomSheetDialogUrutkan;
import com.rti.pasti.menu.home.model.Umroh;
import com.rti.pasti.menu.home.service.UmrohService;
import com.rti.pasti.menu.home.view.fragment.DetailUmrohFragment;
import com.rti.pasti.menu.home.view.fragment.ListUmrohFragment;
import com.rti.pasti.menu.home.view.fragment.PesanUmrohFragment;
import com.rti.pasti.menu.main.adapter.UmrohAdapter;
import com.rti.pasti.util.BottomSheetFragment;
import com.rti.pasti.util.CustomViewPager;
import com.rti.pasti.util.GsonConvert;
import com.rti.pasti.util.RetrofitUtil;
import com.rti.pasti.util.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class UmrohActivity extends AppCompatActivity implements View.OnClickListener {

    CustomViewPager pager;
    private ViewPagerAdapter adapter;

    private ImageView toolbarLeft;
    private ImageView toolbarRight;
    private TextView toolbarTitle;
    private ImageView imgCardTitle;
    private RecyclerView rvUmroh;
    private TextView txtBottomLeft;
    private TextView txtBottomRight;

    private GsonConvert gsonConvert = new GsonConvert();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private UmrohService umrohService;
    private Context context;

    private UmrohAdapter umrohAdapter;
    private List<Umroh.DataBean> umrohList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private CheckBox cboxHargaTerendah, cboxHargaTertinggi, cboxPopularitasTertinggi;
    private BottomSheetFragment bottomSheetFragment = null;
    private ImageView imgHideBottomSheet;
    private Button btnUrutanSelesai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umroh);
        pager = (CustomViewPager)findViewById(R.id.pager);

        configureViewPager();
        umrohService = RetrofitUtil.getInstance().create(UmrohService.class);
        //compositeDisposable = new CompositeDisposable();
        //configureToolbar(getString(R.string.rahn_agen_gadai), getString(R.string.form_arrum_haji));
        pager.setCurrentItem(0);

        context = this;
        //activityUiSetup();

    }

    private void configureViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ListUmrohFragment.newInstance(UmrohActivity.this), ""); //0
        adapter.addFragment(DetailUmrohFragment.newInstance(this), ""); //1
        adapter.addFragment(PesanUmrohFragment.newInstance(this), ""); //2

        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(onPageChangeListener());
    }

    @NonNull
    private ViewPager.OnPageChangeListener onPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1 || position == 2) {
                    //progressBar.setProgress(viewModel.getProgress(3, adapter.getCount()));
                }else if (position > 2){
                    //progressBar.setProgress(viewModel.getProgress(position + 1, adapter.getCount()));
                }else if (position == 0 ){
                    //progressBar.setProgress(viewModel.getProgress(0, adapter.getCount()));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    public void onClick(View view) {
        int currentPosition = pager.getCurrentItem();
        switch (view.getId()){
            case R.id.toolbar_left:
                if (currentPosition == 0)
                    finish();
                else
                    pager.setCurrentItem(currentPosition - 1);
                break;
            case R.id.toolbar_right:
                if (currentPosition == 0)
                    //pencarianLokasiUmroh();
                break;
            case R.id.txtBottomLeft:
                bottomSheetUrutkan();
                break;
            case R.id.txtBottomRight:
                bottomSheetFilter(true);
                break;
            case R.id.imgHideBottomSheet:
                closeBottomSheet();
                break;
            case R.id.btnUrutanSelesai:
                //closeBottomSheet();
                break;
        }
    }

    private static ArrayList<Integer> ar_jumlah_orang = new ArrayList<Integer>();
    public void setJumlahOrang(int jumlahOrang){
        ar_jumlah_orang.clear();
        if (jumlahOrang > 0) {
            for (int i = 1; i <= jumlahOrang; i++) {
                ar_jumlah_orang.add(i);
            }
        }
    }

    public static ArrayList<Integer> getJumlahOrang(){
        if (ar_jumlah_orang.size() <= 0) {
            ar_jumlah_orang.add(0);
        }
        return ar_jumlah_orang;
    }

    private static BottomSheetDialogFilter bottomSheetDialogFilter = null;
    private void bottomSheetFilter(boolean show) {
        if (show) {
            bottomSheetDialogFilter = new BottomSheetDialogFilter();
            bottomSheetDialogFilter.show(getSupportFragmentManager(), bottomSheetDialogFilter.getTag());
        }else {
            bottomSheetDialogUrutkan.dismiss();
        }
    }


    private static BottomSheetDialogUrutkan bottomSheetDialogUrutkan = null;
    private void bottomSheetUrutkan() {
        bottomSheetDialogUrutkan = new BottomSheetDialogUrutkan();
        bottomSheetDialogUrutkan.show(getSupportFragmentManager(), bottomSheetDialogUrutkan.getTag());

    }

    public static void closeBottomSheet(){
        bottomSheetDialogUrutkan.dismiss();
    }

    public void initListUmrohFragment(ImageView toolbarLeft, ImageView toolbarRight, TextView toolbarTitle,
                                      ImageView imgCardTitle, RecyclerView rvUmroh, TextView txtBottomLeft,
                                      TextView txtBottomRight, CheckBox cboxHargaTerendah,
                                      CheckBox cboxHargaTertinggi, CheckBox cboxPopularitasTertinggi,
                                      ImageView imgHideBottomSheet, Button btnUrutanSelesai) {
        this.toolbarLeft = toolbarLeft;
        this.toolbarLeft.setOnClickListener(this);
        this.toolbarRight = toolbarRight;
        this.toolbarRight.setOnClickListener(this);
        this.toolbarTitle = toolbarTitle;
        this.rvUmroh = rvUmroh;
        this.txtBottomLeft = txtBottomLeft;
        this.txtBottomLeft.setOnClickListener(this);
        this.txtBottomRight = txtBottomRight;
        this.txtBottomRight.setOnClickListener(this);
        this.cboxHargaTerendah = cboxHargaTerendah;
        this.cboxHargaTertinggi = cboxHargaTertinggi;
        this.cboxPopularitasTertinggi = cboxPopularitasTertinggi;
        this.imgHideBottomSheet = imgHideBottomSheet;
        this.imgHideBottomSheet.setOnClickListener(this);
        //this.btnUrutanSelesai = btnUrutanSelesai;
        //this.btnUrutanSelesai.setOnClickListener(this);

        //init list UI
        umrohAdapter = new UmrohAdapter(context, umrohList);
        linearLayoutManager = new LinearLayoutManager(context);
        this.rvUmroh.setLayoutManager(linearLayoutManager);
        this.rvUmroh.setItemAnimator(new DefaultItemAnimator());
        this.rvUmroh.setAdapter(umrohAdapter);

        //set jumlah orang
        setJumlahOrang(8);

        //set req & get res
        getAllDataUmroh();
    }


    private void getAllDataUmroh() {
        compositeDisposable.add(
                umrohService.getAll()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Umroh>() {
                            @Override
                            public void accept(Umroh resUmroh) throws Exception {
                                if(resUmroh.getData().size() > 0){
                                    String res = gsonConvert.toJson(resUmroh);
                                    Log.d("resUmroh", String.valueOf(res));
                                    setDataUmroh(resUmroh);
                                }else{
                                    Toast.makeText(context,"Maaf tidak ada data.",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
        );
    }


    private void setDataUmroh(Umroh resUmroh) {
        umrohList.addAll(resUmroh.getData());
        umrohAdapter.notifyDataSetChanged();
    }

    private class myCheckBoxChnageClicker implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            Toast.makeText(UmrohActivity.this, "Murah", Toast.LENGTH_SHORT).show();
        }
    }


    /*@Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.cboxHargaTerendah:
                Toast.makeText(this, "Murah", Toast.LENGTH_SHORT).show();
                break;
        }
    }*/
}
