package com.rti.pasti.menu.akun.model;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public class ResLogin {

    /**
     * message : token_generated
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIzLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3ZlbmRvclwvVU1ST0hcL3Bhc3RpX2FwaVwvcHVibGljXC9hcGlcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTUyNDM3NDQ5NCwiZXhwIjoxNTI0Mzc4MDk0LCJuYmYiOjE1MjQzNzQ0OTQsImp0aSI6InBsazdKRzdSSXpJOGh4NEkifQ.-SfERRAURjbrexsmBGOx1ru1krb4KrGZrny_cyWvBzI
     * data : {"tags_user":6,"source_id":1,"role":11,"email":"anggota.0011.001@gmail.com","phone":"9876425626","name":null,"tabungan":"0.00","referal":"AGEANGTEST001","deposit":"0.00","photo":"http://localhost/vendor/UMROH/pasti_api/publicanggota/default.jpg"}
     */

    private String message;
    private String token;
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        /**
         * tags_user : 6
         * source_id : 1
         * role : 11
         * email : anggota.0011.001@gmail.com
         * phone : 9876425626
         * name : null
         * tabungan : 0.00
         * referal : AGEANGTEST001
         * deposit : 0.00
         * photo : http://localhost/vendor/UMROH/pasti_api/publicanggota/default.jpg
         */

        private int tags_user;
        private int source_id;
        private int role;
        private String email;
        private String phone;
        private Object name;
        private String tabungan;
        private String referal;
        private String deposit;
        private String photo;

        public int getTags_user() {
            return tags_user;
        }

        public void setTags_user(int tags_user) {
            this.tags_user = tags_user;
        }

        public int getSource_id() {
            return source_id;
        }

        public void setSource_id(int source_id) {
            this.source_id = source_id;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getTabungan() {
            return tabungan;
        }

        public void setTabungan(String tabungan) {
            this.tabungan = tabungan;
        }

        public String getReferal() {
            return referal;
        }

        public void setReferal(String referal) {
            this.referal = referal;
        }

        public String getDeposit() {
            return deposit;
        }

        public void setDeposit(String deposit) {
            this.deposit = deposit;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
