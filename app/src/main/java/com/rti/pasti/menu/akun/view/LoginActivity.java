package com.rti.pasti.menu.akun.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rti.pasti.R;
import com.rti.pasti.base.BaseActivity;
import com.rti.pasti.menu.akun.model.ReqLogin;
import com.rti.pasti.menu.akun.model.ResLogin;
import com.rti.pasti.menu.akun.service.AkunService;
import com.rti.pasti.menu.main.view.activity.MainActivity;
import com.rti.pasti.util.RetrofitUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.edUsername)
    EditText edUsername;

    @BindView(R.id.edPassword)
    EditText edPassword;

    private AkunService akunService;
    private CompositeDisposable compositeDisposable;
    private Context context;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        akunService = RetrofitUtil.getInstance().create(AkunService.class);
        compositeDisposable = new CompositeDisposable();
        context = this;
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnLogin)
    void btnLoginOnClick(){
        String username = edUsername.getText().toString();
        String password = edPassword.getText().toString();
        ReqLogin reqLogin = new ReqLogin();
        reqLogin.setUsername(username);
        reqLogin.setPassword(password);
        compositeDisposable.add(
                akunService.login(reqLogin)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ResLogin>() {
                            @Override
                            public void accept(ResLogin resLogin) throws Exception {
                                if(resLogin.getToken().isEmpty() || resLogin.getToken() == ""){
                                    Toast.makeText(LoginActivity.this, getString(R.string.loginFailed), Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(LoginActivity.this, getString(R.string.loginSuccess), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context, MainActivity.class);
                                    startActivity(intent);
                                }
                            }
                        })
        );

    }



}
