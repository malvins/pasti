package com.rti.pasti.menu.main.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.model.Umroh;
import com.rti.pasti.util.NumberConvert;

import java.util.List;

/**
 * Created by firmanmac on 4/24/18.
 */

public class UmrohAdapter extends RecyclerView.Adapter<UmrohAdapter.MyViewHolder> {

    private NumberConvert numberConvert = new NumberConvert();
    private Context context;
    private List<Umroh.DataBean> umrohs;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cv_umroh_item;
        ImageView gambar_umroh;
        TextView txt_umroh_keberangkatan;
        TextView txt_umroh_tanggal;
        TextView txt_umroh_harga;
        TextView txt_nama_paket;
        TextView txt_nama_travel;
        RatingBar rating_umroh;

        public MyViewHolder(View itemView) {
            super(itemView);
            gambar_umroh = (ImageView)itemView.findViewById(R.id.gambar_umroh);
            cv_umroh_item = (CardView)itemView.findViewById(R.id.cv_umroh_item);
            txt_umroh_keberangkatan = (TextView) itemView.findViewById(R.id.txt_umroh_keberangkatan);
            txt_umroh_tanggal = (TextView) itemView.findViewById(R.id.txt_umroh_tanggal);
            txt_umroh_harga = (TextView) itemView.findViewById(R.id.txt_umroh_harga);
            txt_nama_paket = (TextView) itemView.findViewById(R.id.txt_nama_paket);
            txt_nama_travel = (TextView) itemView.findViewById(R.id.txt_nama_travel);
            rating_umroh = (RatingBar)itemView.findViewById(R.id.rating_umroh);
        }
    }

    public UmrohAdapter(Context context, List<Umroh.DataBean> umrohs) {
        this.context = context;
        this.umrohs = umrohs;
    }

    @Override
    public UmrohAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_umroh, parent, false);
        return new UmrohAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UmrohAdapter.MyViewHolder holder, int position) {
        Umroh.DataBean dataBean = umrohs.get(position);
        holder.rating_umroh.setProgress(0);
        holder.txt_nama_paket.setText(dataBean.getPaket_name());
        holder.txt_nama_travel.setText(dataBean.getTravel_name());
        holder.rating_umroh.setProgress(Integer.valueOf((int) dataBean.getRate_total()));
        holder.txt_umroh_keberangkatan.setText(dataBean.getKeberangkatan());
        holder.txt_umroh_tanggal.setText(dataBean.getJadwal());
        holder.txt_umroh_harga.setText(dataBean.getHarga());
        holder.cv_umroh_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "not available yet ...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return umrohs.size();
    }
}
