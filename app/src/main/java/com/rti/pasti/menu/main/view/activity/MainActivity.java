package com.rti.pasti.menu.main.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.rti.pasti.R;
import com.rti.pasti.menu.main.service.MainService;
import com.rti.pasti.util.BottomSheetFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private MainService mainService = new MainService();
    private BottomNavigationViewEx bottomNavigationViewEx;
    private TextView toolbarTitle;
    private ImageView toolbarMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationViewEx = (BottomNavigationViewEx)findViewById(R.id.bottomNavViewBar);
        toolbarTitle = (TextView)findViewById(R.id.toolbar_title);
        toolbarMenu = (ImageView)findViewById(R.id.toolbar_menu);

        setupBottomNavigationView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainService.initSomething(getSupportFragmentManager());
    }

    private void setupBottomNavigationView() {
        mainService.setupBottomNavigationView(bottomNavigationViewEx);
        mainService.enableNavigation(this, bottomNavigationViewEx, toolbarTitle);
        toolbarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment(R.layout.dialog_top_sheet_menu);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
        });
    }

}
