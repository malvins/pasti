package com.rti.pasti.menu.home.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.view.activity.UmrohActivity;

/**
 * Created by firmanmac on 4/24/18.
 */

public class ListUmrohFragment extends Fragment {

    private Context context = null;
    private static UmrohActivity umrohActivity;

    ImageView toolbarLeft;
    ImageView toolbarRight;
    TextView toolbarTitle;
    ImageView imgCardTitle;
    TextView txtCardTitle;
    RecyclerView rvUmroh;
    TextView txtBottomLeft;
    TextView txtBottomRight;
    CheckBox cboxHargaTerendah;
    CheckBox cboxHargaTertinggi;
    CheckBox cboxPopularitasTertinggi;
    ImageView imgHideBottomSheet;
    Button btnUrutanSelesai;

    public static Fragment newInstance(UmrohActivity activity) {
        ListUmrohFragment listUmrohFragment = new ListUmrohFragment();
        umrohActivity = activity;
        return listUmrohFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_umroh_list, container, false);
        context = getActivity();
        toolbarLeft = (ImageView)rootView.findViewById(R.id.toolbar_left);
        toolbarRight = (ImageView)rootView.findViewById(R.id.toolbar_right);
        toolbarTitle = (TextView)rootView.findViewById(R.id.toolbar_title);
        imgCardTitle = (ImageView)rootView.findViewById(R.id.img_card_title);
        txtCardTitle = (TextView)rootView.findViewById(R.id.txt_card_title);
        rvUmroh = (RecyclerView)rootView.findViewById(R.id.rv_umroh);
        txtBottomLeft = (TextView)rootView.findViewById(R.id.txtBottomLeft);
        txtBottomRight = (TextView)rootView.findViewById(R.id.txtBottomRight);
        cboxHargaTerendah = (CheckBox)rootView.findViewById(R.id.cboxHargaTerendah);
        cboxHargaTertinggi = (CheckBox)rootView.findViewById(R.id.cboxHargaTertinggi);
        cboxPopularitasTertinggi = (CheckBox)rootView.findViewById(R.id.cboxPopularitasTertinggi);
        imgHideBottomSheet = (ImageView)rootView.findViewById(R.id.imgHideBottomSheet);
        btnUrutanSelesai = (Button) rootView.findViewById(R.id.btnUrutanSelesai);
        btnUrutanSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(umrohActivity,"AAA",Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        toolbarTitle.setText(getString(R.string.paket_perjalanan_umroh));
        toolbarRight.setImageResource(R.drawable.ic_search_black_24dp);
        imgCardTitle.setImageResource(R.drawable.ic_card_travel_black_24dp);
        txtCardTitle.setText(getString(R.string.pilih_paket_perjalanan_anda));
        txtBottomLeft.setText(getString(R.string.urutkan));
        txtBottomRight.setText(getString(R.string.filter));

        umrohActivity.initListUmrohFragment(toolbarLeft, toolbarRight, toolbarTitle,
                imgCardTitle, rvUmroh, txtBottomLeft, txtBottomRight, cboxHargaTerendah,
                cboxHargaTertinggi, cboxPopularitasTertinggi, imgHideBottomSheet, btnUrutanSelesai);
    }


}
