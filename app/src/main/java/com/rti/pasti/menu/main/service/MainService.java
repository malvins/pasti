package com.rti.pasti.menu.main.service;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.rti.pasti.R;
import com.rti.pasti.menu.main.view.fragment.AkunSayaFragment;
import com.rti.pasti.menu.main.view.fragment.HomeFragment;
import com.rti.pasti.menu.main.view.fragment.NotifikasiFragment;
import com.rti.pasti.menu.main.view.fragment.PembiayaanFragment;
import com.rti.pasti.menu.main.view.fragment.PerjalananFragment;

/**
 * Created by firmanmac on 4/22/18.
 */

public class MainService {

    private FragmentManager supportFragmentManager;
    private Menu menux;
    private MenuItem menuItemx;

    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx){
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(true);
    }

    public void openFragment(final android.support.v4.app.Fragment fragment)   {
        android.support.v4.app.FragmentManager fragmentManager = this.supportFragmentManager;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }

    public void enableNavigation(final Context context, final BottomNavigationViewEx view, final TextView toolbarTitle){
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.ic_home:
                        openFragment(new HomeFragment());
                        menuSelected(view, 0);
                        toolbarTitle.setText("Home");
                        break;

                    case R.id.ic_pembiayaan:
                        openFragment(new PembiayaanFragment());
                        menuSelected(view, 1);
                        toolbarTitle.setText("Pembiayaan");
                        break;

                    case R.id.ic_perjalanan:
                        openFragment(new PerjalananFragment());
                        menuSelected(view, 2);
                        toolbarTitle.setText("Perjalanan");
                        break;

                    case R.id.ic_notifikasi:
                        openFragment(new NotifikasiFragment());
                        menuSelected(view, 3);
                        toolbarTitle.setText("Notifikasi");
                        break;

                    case R.id.ic_akun_saya:
                        openFragment(new AkunSayaFragment());
                        menuSelected(view, 4);
                        toolbarTitle.setText("Akun Saya");
                        break;
                }
                return false;
            }
        });
    }

    private void menuSelected(BottomNavigationViewEx view, int i) {
        menux = view.getMenu();
        menuItemx = menux.getItem(i);
        menuItemx.setChecked(true);
    }


    public void initSomething(FragmentManager supportFragmentManager) {
        this.supportFragmentManager = supportFragmentManager;
        HomeFragment homeFragment = new HomeFragment();
        openFragment(homeFragment);
    }
}
