package com.rti.pasti.menu.akun.model;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public class ReqLogin {
    private String password;
    private String username;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
