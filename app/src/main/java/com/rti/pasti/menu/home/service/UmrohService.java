package com.rti.pasti.menu.home.service;

import com.rti.pasti.conf.MyConf;
import com.rti.pasti.menu.home.model.Umroh;

import io.reactivex.Observable;
import retrofit2.http.POST;

/**
 * Created by M Alvin Syahrin on 4/22/2018.
 */

public interface UmrohService {
    @POST(MyConf.UMROH_API)
    Observable<Umroh> getAll();
}
