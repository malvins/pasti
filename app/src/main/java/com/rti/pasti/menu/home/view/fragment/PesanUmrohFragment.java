package com.rti.pasti.menu.home.view.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rti.pasti.R;
import com.rti.pasti.menu.home.view.activity.UmrohActivity;

/**
 * Created by firmanmac on 4/24/18.
 */

public class PesanUmrohFragment extends Fragment {

    private Context context = null;
    static UmrohActivity umrohActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_akun_saya, container, false);
        getActivity().setTitle("Detail Umroh");
        context = getActivity();
        return rootView;
    }

    public static ListUmrohFragment newInstance(UmrohActivity activity) {
        ListUmrohFragment listUmrohFragment = new ListUmrohFragment();
        umrohActivity = activity;
        return listUmrohFragment;
    }
}
